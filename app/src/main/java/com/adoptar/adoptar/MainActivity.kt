package com.adoptar.adoptar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.adoptar.modules.home.fragments.HomeFragment
import com.adoptar.modules.notifications.fragments.NotificationFragment
import com.adoptar.modules.users.fragments.UserFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val notificationFragment = NotificationFragment.newInstance()
        val homeFragment = HomeFragment.newInstance()
        val userFragment = UserFragment.newInstance()

        bottom_navigation_view.setOnNavigationItemSelectedListener { item: MenuItem ->
             when (item.itemId) {
                R.id.button_nav_action_notifications -> {
                    openFragment(notificationFragment)
                    true
                }
                R.id.button_nav_action_home -> {
                    openFragment(homeFragment)
                    true
                }
                R.id.button_nav_action_profile -> {
                    openFragment(userFragment)
                    true
                }
                else -> false
            }
        }

        openFragment(homeFragment)
        bottom_navigation_view.selectedItemId = R.id.button_nav_action_home
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}